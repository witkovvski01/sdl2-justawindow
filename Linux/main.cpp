/*
    Getting started with SDL2 - Just a Window. 
*/

//For exit()
#include <stdlib.h>

//Include SDL library
#include "SDL2/SDL.h"

int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // SDL allows us to choose which SDL componets are going to be
    // initialised. We'll go for everything for now!
    SDL_Init(SDL_INIT_EVERYTHING);

    gameWindow = SDL_CreateWindow("Hello CIS4008",          // Window title
                              SDL_WINDOWPOS_UNDEFINED,  // X position
                              SDL_WINDOWPOS_UNDEFINED,  // Y position
                              800, 600,                 // width, height
                              SDL_WINDOW_SHOWN);        // Window flags

    // if the window creation succeeded create our renderer
    gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

    //Draw stuff here.

    // 1. Clear the screen
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

    SDL_RenderClear(gameRenderer);

    // 2. Draw the game objects
        // Nothing to do here yet!

    // 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);

    //Pause to allow the image to be seen
    SDL_Delay( 10000 );

    //Clean up!
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);

    //Shutdown SDL - clear up resources etc. 
    SDL_Quit();

    exit(0);
}